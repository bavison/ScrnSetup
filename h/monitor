/* Copyright 1998 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*---------------------------------------------------------------------------*/
/* File:    h.monitor                                                        */
/*---------------------------------------------------------------------------*/

#include <stdbool.h>

typedef struct monitor_ref
{
    struct monitor_ref *next;
    char               *monitor_name;
    char               *file_name;
    ObjectId            menu_id;
    ComponentId         menu_entry;
    short               dpms_state;
    short               origin;
}   monitor_ref;
extern monitor_ref *monitors_list;

void create_screenmodes_instantiation(void);
void discard_screenmodes_instantiation(void);
void create_resolutions_menu(void);
void create_hz_menu(void);
void create_monitors_menu(void);
void update_monitor_displayfields(void);
void update_dpms_status(void);
void switch_resolution(void);
void switch_colour(int menusel);
void switch_monitor(const monitor_ref *rover);

#define FTR_CONFIG 1
#define FTR_SCRAP  2
void read_current_mode_settings(int file_to_read);
#define FTW_NONE   0
#define FTW_CONFIG 1
#define FTW_SCRAP  2
bool set_monitor_choices(int file_to_write);
